import 'package:bloc_template_flutter/core/extensions/bloc_status.dart';
import 'package:bloc_template_flutter/data/data_sources/rickandmorty_api_service.dart';
import 'package:bloc_template_flutter/data/dtos/character_dto.dart';
import 'package:bloc_template_flutter/data/repositories/rickandmorty_repository.dart';
import 'package:bloc_template_flutter/domain/models/character.dart';
import 'package:bloc_template_flutter/presentation/home/blocs/character/character_bloc.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../helpers/dummy/dummy_character_json.dart';
import '../../helpers/hydrated_bloc.dart';

class MockRickandmortyRepository extends Mock
    implements RickandmortyRepository {}

class MockRickandmortyAPI extends Mock implements RickandmortyApiService {}

class MockCharacterBloc extends MockBloc<CharacterEvent, CharacterState>
    implements CharacterBloc {
  @override
  final ScrollController scrollController = ScrollController();
}

void main() async {
  initHydratedBloc();

  late MockRickandmortyRepository mockCharactersRepository;
  late CharacterBloc charactersBloc;
  final mockCharacter = CharacterDTO.fromJson(dummyCharacterJson).toEntity();

  setUp(() {
    mockCharactersRepository = MockRickandmortyRepository();
    charactersBloc = CharacterBloc(mockCharactersRepository);
  });
  group('CharactersBloc', () {
    test('initial state should be [CharactersBloc.state.initial, [] ]', () {
      expect(charactersBloc.state.status.isInitial, true);
      expect(charactersBloc.state.characters, <Character>[]);
    });
  });

  blocTest<CharacterBloc, CharacterState>(
    'test flow',
    build: () {
      when(() => mockCharactersRepository.fetchCharacters(1)).thenAnswer(
        (_) => Future.value([mockCharacter]),
      );
      return charactersBloc;
    },
    act: (bloc) => bloc.add(CharactersFetchEvent()),
    skip: 1,
    expect: () => [
      CharacterState(
        status: BlocStatus.success,
        characters: [mockCharacter],
        page: 1,
      ),
    ],
  );

  blocTest<CharacterBloc, CharacterState>(
    'test flow with fetchContacts event if idNetwork is null',
    build: () {
      when(() => mockCharactersRepository.fetchCharacters(1)).thenThrow(
        Exception(['message']),
      );
      return charactersBloc;
    },
    act: (bloc) => bloc.add(CharactersFetchEvent()),
    expect: () => [
      const CharacterState(
        status: BlocStatus.loading,
      ),
      const CharacterState(
        status: BlocStatus.failure,
      ),
    ],
  );
}
