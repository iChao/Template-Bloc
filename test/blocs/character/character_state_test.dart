import 'package:bloc_template_flutter/core/extensions/bloc_status.dart';
import 'package:bloc_template_flutter/presentation/home/blocs/character/character_bloc.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('CharacterState', () {
    const characterStateJson = {
      'status': 'initial',
      'characters': <Object>[],
      'page': 0,
    };

    const characterState = CharacterState();

    test('fromJson creates a valid CharacterState object', () {
      final result = const CharacterState().fromJson(characterStateJson);
      expect(result, characterState);
    });

    test('toJson creates a valid JSON map', () {
      final result = characterState.toJson(characterState);
      expect(result, characterStateJson);
    });

    test('supports value equality', () {
      final characterState1 =
          const CharacterState().fromJson(characterStateJson);
      final characterState2 =
          const CharacterState().fromJson(characterStateJson);
      expect(characterState1, characterState2);
    });

    test('props contains all fields', () {
      expect(
        characterState.props,
        [
          characterState.status,
          characterState.characters,
          characterState.page,
        ],
      );
    });

    test('copyWith updates status correctly', () {
      const initialState = CharacterState();
      final newState = initialState.copyWith(status: BlocStatus.success);
      expect(newState.status, BlocStatus.success);
      expect(newState.characters, initialState.characters);
      expect(newState.page, initialState.page);
    });

    test('copyWith updates status correctly', () {
      const initialState = CharacterState();
      final newState = initialState.copyWith();
      expect(newState.status, initialState.status);
      expect(newState.characters, initialState.characters);
      expect(newState.page, initialState.page);
    });
  });
}
