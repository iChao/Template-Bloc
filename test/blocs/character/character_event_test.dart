import 'package:bloc_template_flutter/presentation/home/blocs/character/character_bloc.dart';
import 'package:flutter_test/flutter_test.dart';

class _TestCharacterEvent extends CharacterEvent {
  const _TestCharacterEvent();
}

void main() {
  group('contactsEvent Tests', () {
    test('props should return an empty list', () {
      const event = _TestCharacterEvent();
      expect(
        event.props,
        isEmpty,
      );
    });

    test('props should return a list with idNetwork value', () {
      final event = CharactersFetchEvent();
      expect(
        event.props,
        <Object>[],
      );
    });
  });
}
