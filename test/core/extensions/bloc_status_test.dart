import 'package:bloc_template_flutter/core/extensions/bloc_status.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('BlocStatusExtension', () {
    test('isInitial returns true for BlocStatus.initial', () {
      expect(BlocStatus.initial.isInitial, true);
      expect(BlocStatus.loading.isInitial, false);
      expect(BlocStatus.success.isInitial, false);
      expect(BlocStatus.failure.isInitial, false);
    });

    test('isLoading returns true for BlocStatus.loading', () {
      expect(BlocStatus.initial.isLoading, false);
      expect(BlocStatus.loading.isLoading, true);
      expect(BlocStatus.success.isLoading, false);
      expect(BlocStatus.failure.isLoading, false);
    });

    test('isSuccess returns true for BlocStatus.success', () {
      expect(BlocStatus.initial.isSuccess, false);
      expect(BlocStatus.loading.isSuccess, false);
      expect(BlocStatus.success.isSuccess, true);
      expect(BlocStatus.failure.isSuccess, false);
    });

    test('isFailure returns true for BlocStatus.failure', () {
      expect(BlocStatus.initial.isFailure, false);
      expect(BlocStatus.loading.isFailure, false);
      expect(BlocStatus.success.isFailure, false);
      expect(BlocStatus.failure.isFailure, true);
    });
  });
}
