import 'package:bloc_template_flutter/core/enums/character_status.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('CharacterStatusExtension', () {
    test('fromString returns CharacterStatus.alive for "alive"', () {
      expect(
        CharacterStatusExtension.fromString('alive'),
        CharacterStatus.alive,
      );
    });

    test('fromString returns CharacterStatus.unknown for "unknown"', () {
      expect(
        CharacterStatusExtension.fromString('unknown'),
        CharacterStatus.unknown,
      );
    });

    test('fromString returns CharacterStatus.dead for "dead"', () {
      expect(CharacterStatusExtension.fromString('dead'), CharacterStatus.dead);
    });

    test('fromString returns CharacterStatus.empty for unrecognized status',
        () {
      expect(
        CharacterStatusExtension.fromString('other'),
        CharacterStatus.empty,
      );
    });

    test('fromString returns CharacterStatus.empty for empty string', () {
      expect(CharacterStatusExtension.fromString(''), CharacterStatus.empty);
    });

    test('fromString is case insensitive', () {
      expect(
        CharacterStatusExtension.fromString('Alive'),
        CharacterStatus.alive,
      );
      expect(
        CharacterStatusExtension.fromString('UNKNOWN'),
        CharacterStatus.unknown,
      );
      expect(CharacterStatusExtension.fromString('DeAd'), CharacterStatus.dead);
    });
  });
}
