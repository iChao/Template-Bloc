import 'package:bloc_template_flutter/core/enums/character_species.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('CharacterSpeciesExtension', () {
    test('fromString returns CharacterSpecies.human for "human"', () {
      expect(
        CharacterSpeciesExtension.fromString('human'),
        CharacterSpecies.human,
      );
    });

    test('fromString returns CharacterSpecies.alien for "alien"', () {
      expect(
        CharacterSpeciesExtension.fromString('alien'),
        CharacterSpecies.alien,
      );
    });

    test('fromString returns CharacterSpecies.empty for unrecognized species',
        () {
      expect(
        CharacterSpeciesExtension.fromString('other'),
        CharacterSpecies.empty,
      );
    });

    test('fromString returns CharacterSpecies.empty for empty string', () {
      expect(CharacterSpeciesExtension.fromString(''), CharacterSpecies.empty);
    });

    test('fromString is case insensitive', () {
      expect(
        CharacterSpeciesExtension.fromString('Human'),
        CharacterSpecies.human,
      );
      expect(
        CharacterSpeciesExtension.fromString('ALIEN'),
        CharacterSpecies.alien,
      );
      expect(
        CharacterSpeciesExtension.fromString('HuMaN'),
        CharacterSpecies.human,
      );
    });
  });
}
