import 'package:bloc_template_flutter/core/enums/character_gender.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('CharacterGenderExtension', () {
    test('fromString returns CharacterGender.male for "male"', () {
      expect(CharacterGenderExtension.fromString('male'), CharacterGender.male);
    });

    test('fromString returns CharacterGender.female for "female"', () {
      expect(
        CharacterGenderExtension.fromString('female'),
        CharacterGender.female,
      );
    });

    test('fromString returns CharacterGender.unknown for "unknown"', () {
      expect(
        CharacterGenderExtension.fromString('unknown'),
        CharacterGender.unknown,
      );
    });

    test('fromString returns CharacterGender.empty for unrecognized gender',
        () {
      expect(
        CharacterGenderExtension.fromString('other'),
        CharacterGender.empty,
      );
    });

    test('fromString returns CharacterGender.empty for empty string', () {
      expect(CharacterGenderExtension.fromString(''), CharacterGender.empty);
    });

    test('fromString is case insensitive', () {
      expect(CharacterGenderExtension.fromString('Male'), CharacterGender.male);
      expect(
        CharacterGenderExtension.fromString('FEMALE'),
        CharacterGender.female,
      );
      expect(
        CharacterGenderExtension.fromString('UnKnOwN'),
        CharacterGender.unknown,
      );
    });
  });
}
