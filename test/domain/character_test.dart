import 'package:bloc_template_flutter/core/enums/character_gender.dart';
import 'package:bloc_template_flutter/core/enums/character_species.dart';
import 'package:bloc_template_flutter/core/enums/character_status.dart';
import 'package:bloc_template_flutter/domain/models/character.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Character', () {
    const characterJson = {
      'id': 1,
      'name': 'Rick Sanchez',
      'status': 'alive',
      'species': 'human',
      'type': '',
      'gender': 'male',
      'origin': {'name': 'Earth', 'url': ''},
      'location': {'name': 'Earth', 'url': ''},
      'image': 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
    };

    const character = Character(
      id: 1,
      name: 'Rick Sanchez',
      status: CharacterStatus.alive,
      species: CharacterSpecies.human,
      type: '',
      gender: CharacterGender.male,
      origin: {'name': 'Earth', 'url': ''},
      location: {'name': 'Earth', 'url': ''},
      image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
    );

    test('fromJson creates a valid Character object', () {
      final result = Character.fromJson(characterJson);
      expect(result, character);
    });

    test('toJson creates a valid JSON map', () {
      final result = character.toJson();
      expect(result, characterJson);
    });

    test('supports value equality', () {
      final character1 = Character.fromJson(characterJson);
      final character2 = Character.fromJson(characterJson);
      expect(character1, character2);
    });

    test('props contains all fields', () {
      expect(
        character.props,
        [
          character.id,
          character.name,
          character.status,
          character.species,
          character.type,
          character.gender,
          character.origin,
          character.location,
          character.image,
        ],
      );
    });
  });
}
