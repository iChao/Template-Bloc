import 'package:bloc_template_flutter/data/data_sources/rickandmorty_api_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

import '../../helpers/dummy/dummy_characters_json.dart';

void main() {
  final dio = Dio();
  final dioAdapter = DioAdapter(dio: dio);
  final charactersService = RickandmortyApiService(dio: dio);

  group('RickandmortyAPI', () {
    test('characters should make a GET request with page parameter', () async {
      const page = 2;
      const expectedUrl =
          'https://rickandmortyapi.com/api/character/?page=$page';

      dioAdapter.onGet(expectedUrl, (request) {
        return request.reply(200, dummyCharactersJson);
      });

      final characters = await charactersService.characters(page);
      expect(characters.data, dummyCharactersJson);
    });

    test('characters should make a GET request with page parameter', () async {
      const index = 2;
      const expectedUrl = 'https://rickandmortyapi.com/api/character/$index';

      dioAdapter.onGet(expectedUrl, (request) {
        return request.reply(200, dummyCharactersJson);
      });

      final characters = await charactersService.character(index);
      expect(characters.data, dummyCharactersJson);
    });

    test('characters should make a POST request with page parameter', () async {
      const expectedUrl = 'https://rickandmortyapi.com/api/';

      dioAdapter.onPost(expectedUrl, (request) {
        return request.reply(200, dummyCharactersJson);
      });

      final characters = await charactersService.defaultPost({});
      expect(characters.data, dummyCharactersJson);
    });
  });
}
