import 'package:bloc_template_flutter/core/enums/character_gender.dart';
import 'package:bloc_template_flutter/core/enums/character_species.dart';
import 'package:bloc_template_flutter/core/enums/character_status.dart';
import 'package:bloc_template_flutter/data/dtos/character_dto.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('CharacterDTO', () {
    test('can be (de)serialized', () {
      final characterJson = {
        'id': 1,
        'name': 'Rick Sanchez',
        'status': 'alive',
        'species': 'human',
        'type': '',
        'gender': 'male',
        'origin': {
          'name': 'Earth',
          'url': 'https://rickandmortyapi.com/api/location/1',
        },
        'location': {
          'name': 'Earth',
          'url': 'https://rickandmortyapi.com/api/location/1',
        },
        'image': 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
      };

      final characterDTO = CharacterDTO.fromJson(characterJson);

      expect(characterDTO.id, 1);
      expect(characterDTO.name, 'Rick Sanchez');
      expect(characterDTO.status, 'alive');
      expect(characterDTO.species, 'human');
      expect(characterDTO.gender, 'male');

      final characterToJson = characterDTO.toJson();
      expect(characterToJson, characterJson);
    });
    test('can be converted to entity', () {
      final characterJson = {
        'id': 1,
        'name': 'Rick Sanchez',
        'status': 'alive',
        'species': 'human',
        'type': '',
        'gender': 'male',
        'origin': {
          'name': 'Earth',
          'url': 'https://rickandmortyapi.com/api/location/1',
        },
        'location': {
          'name': 'Earth',
          'url': 'https://rickandmortyapi.com/api/location/1',
        },
        'image': 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
      };

      final characterDTO = CharacterDTO.fromJson(characterJson);
      final characterEntity = characterDTO.toEntity();

      expect(characterEntity.id, 1);
      expect(characterEntity.name, 'Rick Sanchez');
      expect(characterEntity.status, CharacterStatus.alive);
      expect(characterEntity.species, CharacterSpecies.human);
      expect(characterEntity.gender, CharacterGender.male);
    });

    test('throws exception when conversion fails', () {
      final characterJson = {
        'id': 1,
        'status': 'alive',
        'species': 'human',
        'gender': 'male',
        'type': '',
        'origin': {
          'name': 'Earth',
          'url': 'https://rickandmortyapi.com/api/location/1',
        },
        'location': {
          'name': 'Earth',
          'url': 'https://rickandmortyapi.com/api/location/1',
        },
        'image': 'https://rickandmortyapi.com/api/character/avatar/1.jpeg',
      };

      final characterDTO = CharacterDTO.fromJson(characterJson);

      expect(characterDTO.toEntity, throwsA(isA<TypeError>()));
    });
  });
}
