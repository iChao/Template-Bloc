import 'package:bloc_template_flutter/data/api/api_exception.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('API Exceptions', () {
    test('InvalidApiKeyException should have correct message', () {
      final exception = InvalidApiKeyException();
      expect(exception.message, 'Invalid API key');
    });

    test('NoInternetConnectionException should have correct message', () {
      final exception = NoInternetConnectionException();
      expect(exception.message, 'No Internet connection');
    });

    test('ResourceNotFoundException should have correct message', () {
      final exception = ResourceNotFoundException();
      expect(exception.message, 'Ressource not found');
    });

    test('UnknownException should have correct message', () {
      final exception = UnknownException();
      expect(exception.message, 'Some error occurred');
    });
  });
}
