import 'dart:io';

import 'package:bloc_template_flutter/data/api/api_exception.dart';
import 'package:bloc_template_flutter/data/data_sources/rickandmorty_api_service.dart';
import 'package:bloc_template_flutter/data/repositories/rickandmorty_repository.dart';
import 'package:bloc_template_flutter/domain/models/character.dart';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../helpers/dummy/dummy_characters_json.dart';

class MockRickandmortyAPI extends Mock implements RickandmortyApiService {}

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  late RickandmortyRepository repository;
  late MockRickandmortyAPI mockApi;
  const page = 1;
  setUp(() {
    mockApi = MockRickandmortyAPI();
    repository = RickandmortyRepository(api: mockApi);
  });

  group('fetchCharacters', () {
    test('returns a list of characters when the API call is successful',
        () async {
      final mockResponse = dummyCharactersJson;

      when(() => mockApi.characters(page)).thenAnswer(
        (_) => Future.value(
          Response(
            data: mockResponse,
            statusCode: 200,
            requestOptions: RequestOptions(),
          ),
        ),
      );

      final characters = await repository.fetchCharacters(page);

      expect(characters, isA<List<Character>>());
      expect(characters.length, 20);
      expect(characters.first.name, 'Rick Sanchez');
    });

    test('throws InvalidApiKeyException when the API call returns 401',
        () async {
      when(() => mockApi.characters(page)).thenAnswer(
        (_) => Future.value(
          Response(
            statusCode: 401,
            requestOptions: RequestOptions(),
          ),
        ),
      );

      expect(
        () => repository.fetchCharacters(page),
        throwsA(isA<InvalidApiKeyException>()),
      );
    });

    test('throws ResourceNotFoundException when the API call returns 404',
        () async {
      when(() => mockApi.characters(page)).thenAnswer(
        (_) => Future.value(
          Response(
            statusCode: 404,
            requestOptions: RequestOptions(),
          ),
        ),
      );

      expect(
        () => repository.fetchCharacters(page),
        throwsA(isA<ResourceNotFoundException>()),
      );
    });

    test('throws UnknownException when the API call returns an unknown error',
        () async {
      when(() => mockApi.characters(page)).thenAnswer(
        (_) => Future.value(
          Response(
            statusCode: 500,
            requestOptions: RequestOptions(),
          ),
        ),
      );

      expect(
        () => repository.fetchCharacters(page),
        throwsA(isA<UnknownException>()),
      );
    });

    test(
        'throws NoInternetConnectionException when there is no internet connection',
        () async {
      when(() => mockApi.characters(page)).thenThrow(const SocketException(''));

      expect(
        () => repository.fetchCharacters(page),
        throwsA(isA<NoInternetConnectionException>()),
      );
    });
  });
}
