import 'dart:io';

import 'package:bloc_template_flutter/data/api/api_exception.dart';
import 'package:bloc_template_flutter/data/data_sources/rickandmorty_api_service.dart';
import 'package:bloc_template_flutter/data/dtos/character_dto.dart';
import 'package:bloc_template_flutter/domain/models/character.dart';

class RickandmortyRepository {
  RickandmortyRepository({required this.api});

  final RickandmortyApiService api;
  Future<List<Character>> fetchCharacters(int? page) {
    return _getData(
      builder: (data) {
        final results = (data as dynamic)['results'] as List<dynamic>;
        final characters = results
            .map(
              (json) => CharacterDTO.fromJson(json as Map<String, dynamic>)
                  .toEntity(),
            )
            .toList();
        return characters;
      },
      page: page,
    );
  }

  Future<T> _getData<T>({
    required T Function(dynamic data) builder,
    int? page,
  }) async {
    try {
      final response = await api.characters(page);
      switch (response.statusCode) {
        case 200:
          final data = response.data;
          return builder(data);
        default:
          _handleHttpError(response.statusCode);
      }
    } on SocketException catch (_) {
      throw NoInternetConnectionException();
    }
    throw UnknownException();
  }

  void _handleHttpError(int? statusCode) {
    switch (statusCode) {
      case 401:
        throw InvalidApiKeyException();
      case 404:
        throw ResourceNotFoundException();
      default:
        throw UnknownException();
    }
  }
}
