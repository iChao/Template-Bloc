import 'package:bloc_template_flutter/data/api/api_interceptor.dart';
import 'package:dio/dio.dart';

class ApiAccessor {
  Dio configureDio(String baseUrl) {
    final dio = Dio(
      BaseOptions(
        baseUrl: baseUrl,
      ),
    );
    dio.interceptors.add(ApiInterceptor(dio));
    return dio;
  }
}
