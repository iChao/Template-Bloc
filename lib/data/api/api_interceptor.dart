// coverage:ignore-file

import 'dart:io';

import 'package:bloc_template_flutter/data/api/headers.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class ApiInterceptor extends Interceptor {
  ApiInterceptor(this.dio);

  Dio dio;

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    if (kDebugMode) {
      print(
        'ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path} Error : ${err.response?.data}',
      );
    }
    super.onError(err, handler);
  }

  @override
  void onResponse(
    Response<dynamic> response,
    ResponseInterceptorHandler handler,
  ) {
    if (kDebugMode) {
      print(
        'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}',
      );
    }
    super.onResponse(response, handler);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    const token = 'TODO : SET TOKEN';
    const headerAuthorization = 'Bearer $token';

    options.headers[ApiHeaders.authorization] = headerAuthorization;
    options.headers
        .putIfAbsent(HttpHeaders.contentTypeHeader, () => 'application/json');

    if (kDebugMode) {
      print('HEADER AUTHORIZATION $headerAuthorization');
      final data = options.data != null ? ' + with DATA : ${options.data}' : '';
      print(
        'REQUEST[${options.method}] => PATH: ${options.baseUrl}${options.path}$data',
      );
    }

    super.onRequest(options, handler);
  }
}
