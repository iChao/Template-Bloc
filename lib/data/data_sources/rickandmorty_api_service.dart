import 'package:dio/dio.dart';

/// Uri builder class for the Rickandmorty API
class RickandmortyApiService {
  RickandmortyApiService({this.dio});
  static const String _sheme = 'https';
  static const String _apiBaseUrl = 'rickandmortyapi.com';
  static const String _apiPath = 'api';
  static const String _pagePath = '/?page=';
  final Dio? dio;

  Future<Response<dynamic>> characters(int? page) => _builRequest(
        method: 'GET',
        endpoint: 'character',
        page: page,
      );

  Future<Response<dynamic>> character(int index) => _builRequest(
        method: 'GET',
        endpoint: 'character/$index',
      );

  Future<Response<dynamic>> defaultPost(Map<String, String> data) =>
      _builRequest(
        method: 'POST',
        endpoint: '',
        parametersBuilder: () => {'data ': 'data'},
      );

  Future<Response<dynamic>> _builRequest({
    required String endpoint,
    required String method,
    int? page,
    Map<String, dynamic> Function()? parametersBuilder,
  }) async {
    final pageString = page != null ? _pagePath + page.toString() : '';
    return (dio ?? Dio()).request(
      '$_sheme://$_apiBaseUrl/$_apiPath/$endpoint$pageString',
      options: Options(method: method),
      queryParameters: parametersBuilder != null ? parametersBuilder() : null,
    );
  }
}
