import 'package:bloc_template_flutter/core/enums/character_gender.dart';
import 'package:bloc_template_flutter/core/enums/character_species.dart';
import 'package:bloc_template_flutter/core/enums/character_status.dart';
import 'package:bloc_template_flutter/domain/models/character.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'character_dto.g.dart';

@JsonSerializable()
class CharacterDTO {
  CharacterDTO({
    required this.id,
    required this.name,
    required this.status,
    required this.species,
    required this.type,
    required this.gender,
    required this.origin,
    required this.location,
    required this.image,
  });

  factory CharacterDTO.fromJson(Map<String, dynamic> json) =>
      _$CharacterDTOFromJson(json);

  Map<String, dynamic> toJson() => _$CharacterDTOToJson(this);

  Character toEntity() {
    try {
      return Character(
        id: id!,
        name: name!,
        status: CharacterStatusExtension.fromString(status!),
        species: CharacterSpeciesExtension.fromString(species!),
        type: type!,
        gender: CharacterGenderExtension.fromString(gender!),
        origin: origin!,
        location: location!,
        image: image!,
      );
    } on Exception catch (e) {
      if (kDebugMode) {
        print(species);
        print(CharacterSpecies.values);
        print('toEntity failure $e');
      }
    }
    throw Exception();
  }

  final int? id;
  final String? name;
  final String? status;
  final String? species;
  final String? type;
  final String? gender;
  final Map<String, String>? origin;
  final Map<String, String>? location;
  final String? image;
}
