import 'package:bloc_template_flutter/core/enums/character_gender.dart';
import 'package:bloc_template_flutter/core/enums/character_species.dart';
import 'package:bloc_template_flutter/core/enums/character_status.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'character.g.dart';

@JsonSerializable()
class Character extends Equatable {
  const Character({
    required this.id,
    required this.name,
    required this.status,
    required this.species,
    required this.type,
    required this.gender,
    required this.origin,
    required this.location,
    required this.image,
  });

  factory Character.fromJson(Map<String, dynamic> json) =>
      _$CharacterFromJson(json);

  Map<String, dynamic> toJson() => _$CharacterToJson(this);

  final int id;
  final String name;
  final CharacterStatus status;
  final CharacterSpecies species;
  final String type;
  final CharacterGender gender;
  final Map<String, String> origin;
  final Map<String, String> location;
  final String image;

  @override
  List<Object?> get props =>
      [id, name, status, species, type, gender, origin, location, image];
}
