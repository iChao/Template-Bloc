// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Character _$CharacterFromJson(Map json) => Character(
      id: (json['id'] as num).toInt(),
      name: json['name'] as String,
      status: $enumDecode(_$CharacterStatusEnumMap, json['status']),
      species: $enumDecode(_$CharacterSpeciesEnumMap, json['species']),
      type: json['type'] as String,
      gender: $enumDecode(_$CharacterGenderEnumMap, json['gender']),
      origin: Map<String, String>.from(json['origin'] as Map),
      location: Map<String, String>.from(json['location'] as Map),
      image: json['image'] as String,
    );

Map<String, dynamic> _$CharacterToJson(Character instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'status': _$CharacterStatusEnumMap[instance.status]!,
      'species': _$CharacterSpeciesEnumMap[instance.species]!,
      'type': instance.type,
      'gender': _$CharacterGenderEnumMap[instance.gender]!,
      'origin': instance.origin,
      'location': instance.location,
      'image': instance.image,
    };

const _$CharacterStatusEnumMap = {
  CharacterStatus.alive: 'alive',
  CharacterStatus.unknown: 'unknown',
  CharacterStatus.dead: 'dead',
  CharacterStatus.empty: 'empty',
};

const _$CharacterSpeciesEnumMap = {
  CharacterSpecies.human: 'human',
  CharacterSpecies.alien: 'alien',
  CharacterSpecies.empty: 'empty',
};

const _$CharacterGenderEnumMap = {
  CharacterGender.male: 'male',
  CharacterGender.female: 'female',
  CharacterGender.unknown: 'unknown',
  CharacterGender.empty: 'empty',
};
