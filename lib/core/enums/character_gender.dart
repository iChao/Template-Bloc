enum CharacterGender { male, female, unknown, empty }

extension CharacterGenderExtension on CharacterGender {
  static CharacterGender fromString(String gender) {
    switch (gender.toLowerCase()) {
      case 'male':
        return CharacterGender.male;
      case 'female':
        return CharacterGender.female;
      case 'unknown':
        return CharacterGender.unknown;
      default:
        return CharacterGender.empty;
    }
  }
}
