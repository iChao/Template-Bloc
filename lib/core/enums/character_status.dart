enum CharacterStatus { alive, unknown, dead, empty }

extension CharacterStatusExtension on CharacterStatus {
  static CharacterStatus fromString(String status) {
    switch (status.toLowerCase()) {
      case 'alive':
        return CharacterStatus.alive;
      case 'unknown':
        return CharacterStatus.unknown;
      case 'dead':
        return CharacterStatus.dead;
      default:
        return CharacterStatus.empty;
    }
  }
}
