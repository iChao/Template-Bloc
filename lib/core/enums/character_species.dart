enum CharacterSpecies { human, alien, empty }

extension CharacterSpeciesExtension on CharacterSpecies {
  static CharacterSpecies fromString(String species) {
    switch (species.toLowerCase()) {
      case 'human':
        return CharacterSpecies.human;
      case 'alien':
        return CharacterSpecies.alien;
      default:
        return CharacterSpecies.empty;
    }
  }
}
