import 'package:bloc_template_flutter/core/extensions/bloc_status.dart';
import 'package:bloc_template_flutter/presentation/core/consts.dart';
import 'package:bloc_template_flutter/presentation/home/blocs/character/character_bloc.dart';
import 'package:bloc_template_flutter/presentation/home/widgets/character_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({required this.title, super.key});
  final String title;
  @override
  Widget build(BuildContext context) {
    final bloc = context.read<CharacterBloc>();
    if (bloc.state.characters.isEmpty) bloc.add(CharactersFetchEvent());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: BlocBuilder<CharacterBloc, CharacterState>(
          builder: (BuildContext context, CharacterState state) {
            return Text('$title (${state.characters.length}) ');
          },
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            BlocBuilder<CharacterBloc, CharacterState>(
              builder: (context, state) {
                return Expanded(
                  child: ListView.builder(
                    itemCount: state.status.isLoading
                        ? state.characters.length + 1
                        : state.characters.length,
                    controller: context.read<CharacterBloc>().scrollController,
                    itemBuilder: (context, index) {
                      if (state.status.isLoading &&
                          index == state.characters.length) {
                        return const Center(
                          child: Padding(
                            padding: EdgeInsets.all(AppSizes.regularPadding),
                            child: SizedBox(
                              width: 50,
                              child: CircularProgressIndicator.adaptive(),
                            ),
                          ),
                        );
                      }
                      return CharacterTile(index: index);
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
