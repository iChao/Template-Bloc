part of 'character_bloc.dart';

@JsonSerializable()
class CharacterState extends Equatable {
  const CharacterState({
    this.status = BlocStatus.initial,
    this.characters = const [],
    this.page = 0,
  });

  final BlocStatus status;
  final List<Character> characters;
  final int page;

  @override
  List<Object?> get props => [status, characters, page];

  CharacterState copyWith({
    BlocStatus? status,
    List<Character>? characters,
    int? page,
  }) {
    return CharacterState(
      status: status ?? this.status,
      characters: characters ?? this.characters,
      page: page ?? this.page,
    );
  }

  CharacterState? fromJson(Map<String, dynamic> json) =>
      _$CharacterStateFromJson(json);

  Map<String, dynamic>? toJson(CharacterState state) =>
      _$CharacterStateToJson(state);
}
