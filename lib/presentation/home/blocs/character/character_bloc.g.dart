// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharacterState _$CharacterStateFromJson(Map json) => CharacterState(
      status: $enumDecodeNullable(_$BlocStatusEnumMap, json['status']) ??
          BlocStatus.initial,
      characters: (json['characters'] as List<dynamic>?)
              ?.map((e) =>
                  Character.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
      page: (json['page'] as num?)?.toInt() ?? 0,
    );

Map<String, dynamic> _$CharacterStateToJson(CharacterState instance) =>
    <String, dynamic>{
      'status': _$BlocStatusEnumMap[instance.status]!,
      'characters': instance.characters.map((e) => e.toJson()).toList(),
      'page': instance.page,
    };

const _$BlocStatusEnumMap = {
  BlocStatus.initial: 'initial',
  BlocStatus.loading: 'loading',
  BlocStatus.success: 'success',
  BlocStatus.failure: 'failure',
};
