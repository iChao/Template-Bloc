import 'package:bloc_template_flutter/core/extensions/bloc_status.dart';
import 'package:bloc_template_flutter/data/repositories/rickandmorty_repository.dart';
import 'package:bloc_template_flutter/domain/models/character.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:json_annotation/json_annotation.dart';

part 'character_bloc.g.dart';
part 'character_events.dart';
part 'character_state.dart';

class CharacterBloc extends HydratedBloc<CharacterEvent, CharacterState> {
  CharacterBloc(this.repository) : super(const CharacterState()) {
    scrollController
        .addListener(onScroll); //shouldn't be here but easier to use
    on<CharactersFetchEvent>(_charactersFetchEvent);
  }
  final RickandmortyRepository repository;
  final ScrollController scrollController = ScrollController();

  Future<void> _charactersFetchEvent(
    CharactersFetchEvent event,
    Emitter<CharacterState> emit,
  ) async {
    try {
      if (state.status.isLoading) return;
      emit(state.copyWith(status: BlocStatus.loading));
      final characters = await repository.fetchCharacters(state.page + 1);
      final newList = state.characters + characters;
      emit(
        state.copyWith(
          status: BlocStatus.success,
          characters: newList,
          page: state.page + 1,
        ),
      );
    } on Exception catch (_) {
      emit(state.copyWith(status: BlocStatus.failure));
    }
  }

  void onScroll() {
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      add(CharactersFetchEvent());
    }
  }

  @override
  CharacterState? fromJson(Map<String, dynamic> json) =>
      _$CharacterStateFromJson(json);

  @override
  Map<String, dynamic>? toJson(CharacterState state) =>
      _$CharacterStateToJson(state);
}
