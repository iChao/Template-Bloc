import 'package:bloc_template_flutter/presentation/core/consts.dart';
import 'package:bloc_template_flutter/presentation/home/blocs/character/character_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CharacterTile extends StatelessWidget {
  const CharacterTile({
    required this.index,
    super.key,
  });

  final int index;

  Widget hSeparator() {
    return const SizedBox(height: 5);
  }

  Widget wSeparator() {
    return const SizedBox(width: 20);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CharacterBloc, CharacterState>(
      builder: (context, state) {
        final character = state.characters[index];

        return Padding(
          padding: const EdgeInsets.all(AppSizes.halfPadding),
          child: Card(
            elevation: AppSizes.regularElevation,
            child: Padding(
              padding: const EdgeInsets.all(AppSizes.halfPadding),
              child: Row(
                children: [
                  Center(
                    child: CircleAvatar(
                      radius: AppSizes.extendedRadius,
                      backgroundImage: NetworkImage(character.image),
                    ),
                  ),
                  wSeparator(),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          character.name,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                        hSeparator(),
                        Text(
                          '$speciesLeadingLabel ${character.species.name}',
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                        hSeparator(),
                        Text(
                          '$statusLeadingLabel ${character.status.name}',
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                        hSeparator(),
                        Text(
                          '$genderLeadingLabel ${character.gender.name}',
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
