import 'package:bloc_template_flutter/data/api/api_accessor.dart';
import 'package:bloc_template_flutter/data/data_sources/rickandmorty_api_service.dart';
import 'package:bloc_template_flutter/data/repositories/rickandmorty_repository.dart';
import 'package:bloc_template_flutter/presentation/home/blocs/character/character_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BaseComponent extends StatelessWidget {
  const BaseComponent({required this.child, super.key});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final api = ApiAccessor();
    final dio = api.configureDio('');
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => CharacterBloc(
            RickandmortyRepository(api: RickandmortyApiService(dio: dio)),
          ),
        ),
      ],
      child: child,
    );
  }
}
