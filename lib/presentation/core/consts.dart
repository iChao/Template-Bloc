String speciesLeadingLabel = 'Species :';
String statusLeadingLabel = 'Status :';
String genderLeadingLabel = 'Gender :';

class AppSizes {
  /* -- Padding -- */
  static const double tinyPadding = 2;
  static const double smallPadding = 4;
  static const double halfPadding = 8;
  static const double twoThirdPadding = 12;
  static const double regularPadding = 16;
  static const double largePadding = 24;
  static const double doublePadding = 32;
  static const double extendedPadding = 48;
  static const double quadruplePadding = 64;
  static const double bottomSafePadding = 128;

  /* -- Radius -- */
  static const double smallRadius = 4;
  static const double halfRadius = 8;
  static const double regularRadius = 16;
  static const double doubleRadius = 32;
  static const double extendedRadius = 46;

  /* -- Text -- */
  static const double regularFontSize = 16;
  static const double doubleFontSize = 34;

  /* -- Elevation -- */
  static const double halfElevation = 8;
  static const double regularElevation = 16;

  static const double doubleElevation = 32;
}

class AppAnimationTime {
  static const Duration textStyle = Duration(milliseconds: 250);
  static const Duration arrowRotation = Duration(milliseconds: 100);
  static const Duration carousel = Duration(milliseconds: 500);
}
