import 'package:flutter/material.dart';

TextTheme customTextTheme() {
  return const TextTheme(
    displayLarge: TextStyle(
      fontSize: 72,
      fontWeight: FontWeight.bold,
    ),
    titleLarge: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    ),
    titleMedium: TextStyle(color: Colors.black),
    headlineLarge: TextStyle(color: Colors.black),
    bodyMedium: TextStyle(color: Colors.black),
    displaySmall: TextStyle(),
  );
}
