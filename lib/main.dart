import 'dart:convert';

import 'package:bloc_template_flutter/presentation/core/base_component.dart';
import 'package:bloc_template_flutter/presentation/core/custom_theme.dart';
import 'package:bloc_template_flutter/presentation/home/views/home_page.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory:
        HydratedStorageDirectory((await getTemporaryDirectory()).path),
    encryptionCipher: HydratedAesCipher(
      sha256.convert(utf8.encode('insaneEncodage')).bytes,
    ),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BaseComponent(
      child: MaterialApp(
        title: 'Bloc Template',
        theme: ThemeData(
          useMaterial3: true,
          textTheme: customTextTheme(),
        ),
        home: const HomePage(title: 'Bloc Template Home Page'),
      ),
    );
  }
}
