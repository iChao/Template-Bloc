# Template Projet Flutter

Ce projet a été créé le 15/01/25 avec Flutter **v3.27.2**.  

Si vous utilisez ce template et qu'il n'a pas été mis à jour depuis longtemps, il est possible que certaines implémentations soient obsolètes.

## Comment utiliser ce projet
1 - Clonez le projet  
2 - Supprimez le fichier .git  
3 - Renommez le package `fr.norsys.bloc_template_flutter` par votre nom de package  
4 - Changez le nom de l'application dans le `pubspec.yaml`  
5 - A vous de jouer

## Comment générer les fichiers requis JsonSerializable ?

`flutter pub run build_runner build --delete-conflicting-outputs `

## Structure du Template

![](structure.png)  
- **main.dart** : Le point d'entrée de votre application.  
- **presentation** : Le package contenant des sous-dossiers correspondant chacun à un écran de votre app.  
- **core** : Le package qui contient tous les éléments qui seront utilisés à plusieurs endroits dans votre app.  
- **data** : Le package qui contient tous les éléments en lien avec un modèle donnée du serveur ou une API.
- **services** : La couche qui s'occupe des échanges avec un gestionnaire de données (API ou BDD) de manière générale. 
- **domain** : 
    - **models** : La couche qui s'occupe des échanges avec un gestionnaire de données (API ou BDD)  
## Librairies utilisées
- State management : [la librairie Bloc](https://pub.dev/packages/bloc)  
- Cache Data : [la librairie HydratedBloc](https://pub.dev/packages/hydrated_bloc)  
- Tests : [la librairie Bloc_Test](https://pub.dev/packages/bloc_test)  
- Echanges réseau : [Dio](https://pub.dev/packages/dio)
- Serialisation : [Json Serializable](https://pub.dev/packages/json_serializable) et [Json Annotation] (https://pub.dev/packages/json_annotation)  
